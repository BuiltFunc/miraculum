const path = require("path");
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  // 'development' | 'production'
  devtool: 'source-map',
  // entry points -> ts ファイルを指定（複数可）
  // https://webpack.js.org/concepts/entry-points/#multi-page-application
  entry: {
    "main": path.resolve(__dirname, "./public/src/ts/Main.ts")
  },
  output: {
    path: path.resolve(__dirname, "./public/assets/js"),
    filename: "[name].bundle.js"
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader?cacheDirectory',
          },
          {
            loader: 'ts-loader',
          },
        ],
      }
    ]
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      terserOptions: {
        /**
         * TODO: コメントアウト
        compress: {
          drop_console: true,
        },
         */
        output: {
          comments: false,
        },
      },
    })],
  }
};