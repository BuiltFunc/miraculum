window.ggBrandName = 'GGPOKER';
var ggDownloadMobile = 'https://download.good-game-network.com/mobile-page/' + window.ggBrandName + '/en';
var ggDownloadWindows = 'https://download.good-game-network.com/desktop/installer/windows/' + window.ggBrandName + '/en';
var ggDownloadMacOS = 'https://download.good-game-network.com/desktop/installer/osx/' + window.ggBrandName + '/en';
var llink = '';

function downloadPokerClient(e, link) {
  e.preventDefault();
  ga('send', 'event', 'Download - GGS Landing', 'Click', 'Event', 1);
  if (link) {
    window.location.href = link;
  }
}

jQuery(window).bind("load", function (e) {
  var buttonObj = jQuery('#ggDownload-button');
  var buttonHtml = '';
  switch (device.os) {
    case 'windows':
      link = ggDownloadWindows;
      buttonHtml = '<a href="#" class="fdb1 btn btn-primary">DOWNLOAD NOW</a>';
      downloadPokerClient(e, link);
      break;
    case 'macos':
      link = ggDownloadMacOS;
      buttonHtml = '<a href="#" class="fdb1 btn btn-primary">Download Free App Now</a>';
      downloadPokerClient(e, link);
      break;
    case 'android':
    case 'ios':
      link = ggDownloadMobile;
      buttonHtml = '<a href="#" class="fdb1 btn btn-primary">Download Free App Now</a>';
      break;
    default:
      break;
  }
  if (buttonHtml) {
    buttonObj.html(buttonHtml).ready(function () {
      jQuery('#ggDownload-button a').on('click', function (e) {
        downloadPokerClient(e, link);
      });
    });
  }
});